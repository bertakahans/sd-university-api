<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Basic User info from API
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Courses list
Route::middleware('auth:api')->get('/courses', 'Api\CourseController@getCourses');
// Register User to Course
Route::middleware('auth:api')->post('/courses/{course}/users', 'Api\RegistrationController@registerUserToCourse');

// User creation
Route::post('/users','Api\UserController@create');
// User login
Route::post('/session','Api\UserController@login');