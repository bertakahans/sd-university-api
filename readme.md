# Code Challenge Summary

This is a demo API for a code contest purpose. Based on Laravel 5.8 and using OAuth2 included as a package of it.


## API Functionality

This application aims to respond to the following requisites, simulating a simple university/school context:
- Create user/student account
- Offer an API Login endpoint
- Register user/student to a course
- Retrieve a list of courses and their availability


## Setup 

Run composer specific installation for dependencies: "composer install"

Update Laravel ".env". Check ".example" and "php artisan key:generate" for APP-KEY.


## Run
The project is based on Laravel 5.8 under a homestead Vagrant box.

After checking the "Folder Sync" and ip for current project on Homestead.yaml, it is possible to run vagrant up to get the application running.

- Check your local /etc/hosts file for correct DNS mapping.
- Check project's Homestead.yaml for correct ip and folder mappings. Check ".example".
- Run "vagrant up" command

Additionally, run migrations and seedings from vagrant box:

- Access to box through ssh: vagrant ssh
- Run artisan commands:
	- php artisan migrate
	- php artisan db:seed

## Documentation for API calls is present on next URL:
Served By Postman: https://documenter.getpostman.com/view/6893730/SVYnSMHr?version=latest#62680789-0fd6-4bc0-b15b-a5b61b2280f4
