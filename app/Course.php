<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'capacity',
    ];

    /**
     * Relation between Course and Users
     *
    */
    public function users(){
        return $this->belongsToMany(User::class,'registration');
    }
}
