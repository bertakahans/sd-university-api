<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new user and return corresponding 
     * token as a response if valid.
     *
     * @param  Request  $request
     * @return Acces token
     */
    public function create(Request $request){
    	// Validate incoming data or fail
        $validator = Validator::make($request->all(),[
            'email' => 'required|email|max:255|unique:users',
            'name' => 'required|max:255',
            'password' => 'required|min:8|confirmed'
        ]);

        // Check validator to send response
        if( $validator->fails() ){
        	// TO DO: Show more informative message 
        	return response(['result'=>json_encode('Existing User or Wrong Fields Information',true)]);
        }

        // Create new User instance
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

		return response([
			'access_token'=>$this->getUserAccessToken($request)
		]);

    }

    /**
     * Return API access token if user is authenticated.
     *
     * @param  Request  $request
     * @return Acces token
     */
    public function login(Request $request){

        return response([
        	'access_token'=>$this->getUserAccessToken($request)
		]);
    }

    /**
     * Get access token for registered user
     *
     * @param  User  $user
     * @return Acces token
     */
    private function getUserAccessToken($user){
    	
    	$guzzle = new Client;

    	// TO DO: Check correct alternatives
    	$oauthGrantCli = DB::table('oauth_clients')->where('name','SDUniversity Password Grant Client')->first();
    	
    	// If oauth granting client exists, use it to login
    	if($oauthGrantCli!=null){

    		// Curl call for authentication
			$response = $guzzle->post( url('/oauth/token'), [
			    'form_params' => [
			        'grant_type' => 'password',
			        'client_id' => $oauthGrantCli->id,
			        'client_secret' => $oauthGrantCli->secret,
			        'username' => $user->email,
	        		'password' => $user->password,
	        		'scope' => '',
			    ],
			]);

			return json_decode((string) $response->getBody(), true)['access_token'];
		}else {
			return 'Unauthorized';
		}


    }

}
