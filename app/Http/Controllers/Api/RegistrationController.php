<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\Course;

class RegistrationController extends Controller
{
    /**
     * Return Courses List with availability check.
     *
     * @param  Request $request
     * @return Acces token
     */
    public function registerUserToCourse(Request $request){

    	// Check for incoming course_id
    	if ( $request->course != null){

    		// Get course
    		$course = Course::find($request->course);
    		if($course != null){

    			// Check availability
    			$registeredUsers = $course->users()->count();
	    		if($course->capacity > $registeredUsers){
	    			
	    			// Get Authenticated User
	    			$user = Auth::user();

	    			if(!$user->courses->contains($course->id)){
	    				// Sync Course with User without breaking previous relations
		    			$currentDateTime = date('Y-m-d H:i:s');
		    			$user->courses()->syncWithoutDetaching([
		    				$course->id => [ 'registered_on' => $currentDateTime ]
		    			]);

		    			// Return OK
		        		$message = $user->name . ' registered on: '. $course->name.' (id: '.$course->id.')';
	    			}else{

		    			// Return Hint
		        		$message = $user->name . ' was previously registered for '. $course->name .' (id: '.$course->id.')';
	    			}

	    			

				}else{
					$message = 'Could not register more user on: '. $course->name;
				}

			}else{
				$message = 'Course '. $request->course .'does not exist...';
			}
    	}else{
    		$message = 'Incorrect URL call, check course_id...';
    	}

		return response(['result'=>  $message ]);
    }


}
